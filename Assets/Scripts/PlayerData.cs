using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum GameStates
{
    Normal,
    Growing,
    Giant,
    GoingDown
};
public enum Types {

    Mage,
    Warrior,
    Paladin,
    Assassin

}




public class PlayerData : MonoBehaviour
{

    public string PlayerName;
    public float Height;
    public float Weight = 10f;
    public GameStates PlayerState = GameStates.Normal;
    public string PlayerType;
    public float Speed;
    public float DistanceTraveled;
    public Transform _PlayerTransform;


    [SerializeField]
    private Sprite[] _sprite;
    

    public Transform MyTransform;
    public int imageIndex = 0;
    
    
    private SpriteRenderer _sr;


    private float _speedAtenuator=0.01f;
    private float _time;
    [SerializeField]
    private float _frame = 0;
    [SerializeField]
    private float _totalFramePerSecond = 12;

    private float _coolDownTime = 0;
    private float _giantValue = 4;
    private float _giantTime = 0f;
    private float _coolDownValue = 3f;

    private float _count=0;

    private float _playerPositionInX;
    private float _maxDistance;
    private float _minDistance;
    private float _stopTime=0f;
    private float _StopcoolDownTime = 0;




    private void Awake()
    {
        PlayerName = Controler.PlayerName;
        PlayerType = Controler.PlayerType;
        Height = Controler.PlayerHeight;
        Speed = Controler.PlayerSpeed;
        DistanceTraveled = Controler.PlayerDistance;
        
        PlayerState = GameStates.Normal;

        _PlayerTransform = GameObject.Find("Guard").transform;
        _playerPositionInX = _PlayerTransform.position.x;
        _maxDistance =  transform.position.x - DistanceTraveled;
        _minDistance = transform.position.x + DistanceTraveled;
        
    }

    // Start is called before the first frame update

    void Start()
    {
        _sr = GetComponent<SpriteRenderer>();
     
    }

    //Update is called once per frame To move the objeto to the left
    void Update()
    {

        Height = GameObject.Find("Guard").GetComponent<PlayerData>().Height;
        _giantValue = Height/2;
        _maxDistance = _playerPositionInX - DistanceTraveled;
        _minDistance = _playerPositionInX + DistanceTraveled;

        PrintSpritesPerFrame();
        ChangeState();
        //ChangeSpeed();
        IndependentMovement();
        MoveNameOverNpc();
        UpdateCountFrame();
        MoveTypeOverNpc();

    }

    private void PrintSpritesPerFrame() 
    {
        _time += Time.deltaTime;

        if (_time >= 1 / _totalFramePerSecond)
        {
            _frame++;
            _time = 0;
            if (_frame == 3)
            {
                _frame = 0;

                if (imageIndex < _sprite.Length - 1)
                {
                    _sr.sprite = _sprite[imageIndex];
                    imageIndex++;
                }
                else
                {
                    _sr.sprite = _sprite[imageIndex];
                    imageIndex = 0;
                }

            }

        }
    }

    private void UpdateCountFrame() 
    {
        
        
        if (_count < 60) 
        {
            _count++;
        }

        if (_count == 60) {

            GameObject.Find("FramesPerSecond").GetComponent<Text>().text = "Frame Rate: " + (1 / Time.deltaTime).ToString();
            _count = 0;


        }
        
            

   
    }
    private void ChangeState()
    {
        if (PlayerState == GameStates.Normal) _coolDownTime += Time.deltaTime;

        

        if (_coolDownTime > _coolDownValue)
        {

            if (Input.GetKeyDown("space"))
            {

                _coolDownTime = 0f;

                PlayerState = GameStates.Growing;

                GameObject.Find("CoolDownAlert").GetComponent<Text>().text = "";

            }
            else 
            {
                GameObject.Find("CoolDownAlert").GetComponent<Text>().text = "Press spacebar to make it giant!!";

            }
        }

        if(PlayerState == GameStates.Growing) _giantTime += Time.deltaTime;

        if (_giantTime > _giantValue)
        {

            if (PlayerState == GameStates.Growing)
            {
                _giantTime = 0;
                PlayerState = GameStates.Giant;
            }


        }
    }

    private void IndependentMovement()
    {

        if (_PlayerTransform.rotation.y == 0f)
        {
            _StopcoolDownTime += Time.deltaTime;
          
           

            if (_StopcoolDownTime > _stopTime)
            {


                if (transform.position.x > _maxDistance)
                {
                    
                    transform.position = new Vector3(Speed * -_speedAtenuator + transform.position.x, transform.position.y, transform.position.z);
                    
                }
                else
                {
                    Quaternion target = Quaternion.Euler(0, 180f, 0f);
                    _PlayerTransform.rotation = target;
                    _stopTime = 5f;
                    _StopcoolDownTime = 0f;
                }

            }
        }

        if (_PlayerTransform.rotation.y < 0f)
        {

            
            _StopcoolDownTime += Time.deltaTime;

           

            if (_StopcoolDownTime > _stopTime)
            {

                if (transform.position.x < _minDistance)
                {
                    

                    transform.position = new Vector3(Speed * _speedAtenuator + transform.position.x, transform.position.y, transform.position.z);
                    
                }
                else
                {
                    Quaternion target = Quaternion.Euler(0, 0f, 0f);
                    _PlayerTransform.rotation = target;
                    _StopcoolDownTime = 0f;
                    _stopTime = 5f;
                }

            }
        }
    }

    private void MoveNameOverNpc() 
    {
        GameObject _textNameGO = GameObject.Find("PlayerNameFollow");
       
        Camera MainCamera = Camera.main;

        _textNameGO.GetComponent<RectTransform>().anchoredPosition = MainCamera.WorldToScreenPoint(transform.position);
        
    }
    private void MoveTypeOverNpc() {
        GameObject _textTYPEGO = GameObject.Find("TypeNPC");
        Camera MainCamera = Camera.main;
        _textTYPEGO.GetComponent<Text>().text = PlayerType;
        _textTYPEGO.GetComponent<RectTransform>().anchoredPosition = MainCamera.WorldToScreenPoint(transform.position);
    }

}
