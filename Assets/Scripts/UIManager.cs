using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIManager : MonoBehaviour
{
    private GameObject _player;
    private Button _button;
    private GameObject _startGame;
   


    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("Player");

        _player.SetActive(false);

        _startGame = GameObject.Find("GameStart");

        _button = GameObject.Find("StartButton").GetComponent<Button>();

        _button.onClick.AddListener(ActivePanels);
    }




    void ActivePanels() {
        _startGame.SetActive(false);
        _player.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {


        
    }
}
