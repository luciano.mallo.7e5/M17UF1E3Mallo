using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrintNameDataPlayer : MonoBehaviour
{

    //PlayerData player;
    private GameObject _player;
    private PlayerData _playerData;




    // Start is called before the first frame update
    void Start()
    {

        _player = GameObject.Find("Guard");
        _playerData = _player.GetComponent<PlayerData>();
        this.gameObject.GetComponent<Text>().text = _playerData.PlayerName;

    }

    // Update is called once per frame
    void Update()
    {
      


    }
}
