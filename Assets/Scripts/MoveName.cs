using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveName : MonoBehaviour
{

    private GameObject _playerToFollowOG;
    public Vector3 _NpcPosition;
    private GameObject _textGO;
    public Transform MyTransform;
    public Camera MainCamera;




    // Start is called before the first frame update
    void Start()
    {
        _playerToFollowOG = GameObject.Find("Guard");
        
        MainCamera = Camera.main;



        _textGO = GameObject.Find("PlayerNameFollow");
       

    }

    // Update is called once per frame
    void Update()
    {
        _textGO.GetComponent<RectTransform>().position = MainCamera.ScreenToWorldPoint(_playerToFollowOG.transform.position);

    }
}
