using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeItGiant : MonoBehaviour
{
   private GameObject _player;
   private PlayerData _playerData;
   private Vector3 _scaleChange= new Vector3(+0.01f, +0.01f, 0.0f);
   private Vector3 _maxScale;
   private float _proportionScale = 5f;
    private float _weight;
    private float _maxHeight;
    private float _originalSpeed;

    private void Awake()
    {
        _player = GameObject.Find("Guard");
        _playerData = GameObject.Find("Guard").GetComponent<PlayerData>();
        _originalSpeed = _playerData.Speed;
    }
    // Start is called before the first frame update
    void Start()
    {
        
        _weight = _playerData.Weight;
        _maxHeight = _playerData.Height * _proportionScale;
        _maxScale = new Vector3(_maxHeight, _maxHeight, _proportionScale);
        

    }

    // Update is called once per frame
    void Update()
    {
        _maxHeight = _playerData.Height * _proportionScale;
        _maxScale = new Vector3(_maxHeight, _maxHeight, _proportionScale);

        MakeGiantOrNormal();
       

    }


    private void MakeGiantOrNormal() {


        
        switch (_playerData.PlayerState) {

            case GameStates.Normal:

                break;

            case GameStates.GoingDown:

                if (_playerData.Weight != _weight)
                {
                    _player.transform.localScale -= _scaleChange;
                    _playerData.Weight -= 1f;

                    _playerData.Speed = _playerData.Speed / _playerData.Weight;
                }
                else
                {
                    _playerData.Speed = _originalSpeed;
                    _playerData.PlayerState = GameStates.Normal;
                }

                break;

            case GameStates.Growing:
               
                    _player.transform.localScale += _scaleChange;
                    _playerData.Weight += 1f;
                    _playerData.Speed = _playerData.Speed / _playerData.Weight;

                break;


            case GameStates.Giant:

                _playerData.PlayerState = GameStates.GoingDown;

                break;
        
        }

    }
   


}
