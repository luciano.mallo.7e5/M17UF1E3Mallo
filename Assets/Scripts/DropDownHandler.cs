using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DropDownHandler : MonoBehaviour
{
    Dropdown _dropDown;

    // Start is called before the first frame update
    void Start()
    {
        _dropDown = transform.GetComponent<Dropdown>();

        _dropDown.options.Clear();

        foreach (Types type in Enum.GetValues(typeof(Types)))
        {
            _dropDown.options.Add(new Dropdown.OptionData() { text = type.ToString() });
        }

        _dropDown.onValueChanged.AddListener(delegate { DropdownItemSelected(_dropDown); });



    }

    void DropdownItemSelected(Dropdown dropdown) 
    {
        int index = dropdown.value;


        Controler.PlayerType = dropdown.options[index].text;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
